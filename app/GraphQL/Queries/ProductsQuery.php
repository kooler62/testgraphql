<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use App\Models\Product;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

final class ProductsQuery
{
    public function paginate($root, array $args): LengthAwarePaginator
    {
        return Product::query()->paginate(perPage: $args['count'], page: $args['page']);
    }
}
